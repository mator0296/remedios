/** @format */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, TextInput } from 'react-native';
import { Spinkit } from '@components';
import { Facebook, Google } from '@expo';
import { Events, Tools, Config, Languages } from '@common';
import User from '@services/User';
import Icon from '@expo/vector-icons/FontAwesome';
import { fetchUserData } from '@redux/actions';
import { connect } from 'react-redux';
import css from './style';
import Entypo from 'react-native-vector-icons/Entypo'

class SignIn extends PureComponent {
  static propTypes = {
    fetchUserData: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      appLoading: false,
      email: '',
      password: ''
    };
  }

  btnLogIn = () => {
    

    if (this.state.email !== '') {
      this.setState({ loading: true });
      User.recoveryPassword(
        this.state.email.trim(),
        () => {
          
          Events.loginShowInfo('En su correo encontrara un enlace para recuperar su contraseña');
          this.setState({ loading: false, email:'' });
        },
        (error) => {
          
          this.setState({ loading: false});
          Events.loginShowError('No se encuentra usuario registrado con este correo');
        }
      );
    }
  };

  

 

  render() {
    if (this.state.appLoading) {
      return <Spinkit size={30} type="FadingCircle" color="#FFFFFF" />;
    }

    return (
      <View style={css.wrap}>
        <View style={css.body}>
        
       
          <View style={css.wrapForm}>
            <View style={css.textInputWrap}>
              <Text style={css.textLabel}>{"INTRODUCE TU CORREO Y TE ENVIAREMOS LAS INSTRUCCIONES PARA RECUPERAR TU CONTRASEÑA"}</Text>
              <TextInput
                placeholder={Languages.enterEmail}
                underlineColorAndroid="transparent"
                style={css.textInput}
                value={this.state.email}
                onChangeText={(text) => this.setState({ email: text})}
              />
            </View>
          </View>

          <View style={css.wrapButton}>
            {this.state.loading ? (
              <TouchableOpacity style={css.btnLogIn}>
                <Spinkit />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity style={css.btnLogIn} onPress={this.btnLogIn}>
                <Text style={css.btnLogInText}> {'Recuperar contraseña'} </Text>
              </TouchableOpacity>
            )}
            <TouchableOpacity onPress={this.props.changeRecevery}>
              <Text style={{color: '#333', fontWeight: 'bold', fontSize: 14, marginBottom:'5%'}}>{'¿Ya eres miembro? '}<Text style={{color: 'blue', fontSize: 14, textAlign: 'right', textDecorationLine:'underline'}}>{'Inicia sesión'}</Text> </Text>
            </TouchableOpacity> 
            <TouchableOpacity onPress={()=>this.props.goToPage(1)}>
              <Text style={{color: '#333', fontWeight: 'bold', fontSize: 14, marginBottom:'5%'}}>{'¿Eres nuevo? '}<Text style={{color: 'blue', fontSize: 14, textAlign: 'right', textDecorationLine:'underline'}}>{'Regístrate'}</Text> </Text>
            </TouchableOpacity>  
          </View>
          
        </View>
      </View>
    );
  }
}

export default connect(
  null,
  { fetchUserData }
)(SignIn);
