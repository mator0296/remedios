/** @format */

import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, TextInput } from 'react-native';
import { Tools, Events, Languages, Config } from '@common';
import { Spinkit } from '@components';
import User from '@services/User';
import Icon from '@expo/vector-icons/FontAwesome';
import css from './styles';

export default SignUp = (props)=> {
    
    return (
      <View style={css.wrap}>
        <View style={css.body}>
          <View style={css.wrapForm}>
            <View style={css.textInputWrap}>
              <Icon name='check-circle' size={100} style={{color:'rgb(72,194,172)', textAlign:'center', marginBottom:'5%'}}></Icon>
              <Text style={[css.textLabel,{fontWeight:'100', fontSize:15,textAlign:'center'}]}><Text style={{fontWeight:'bold'}}>{'Paso 1: '}</Text>{ 'Para finalizar el registro abre tu correo electrónico'}</Text>
              <Text style={[css.textLabel, {textAlign:'center', fontSize:15}]}>{props.email}</Text>
              <Text style={[css.textLabel, {textAlign:'center', fontSize:15, fontWeight:'100'}]}>{'Y sigue las instrucciones  en el correo.'}</Text>
              <Text style={[css.textLabel, {textAlign:'center', fontSize:15, fontWeight:'100'}]}>{'Si no ves el correo en la bandeja de entrada búscalo  en la carpeta de no deseados y márcalo como "Correo Deseado"'}</Text>
              <Text style={[css.textLabel,{fontWeight:'100', fontSize:15,textAlign:'center'} ]}><Text style={{fontWeight:'bold'}}>{'Paso 2: '}</Text>{ 'Cuando finalices las instrucciones en el correo presiona el siguiente botón:'}</Text>
            </View>
            <View style={css.wrapButton}>
              <TouchableOpacity style={css.btnLogIn} onPress={props.goToLogin}>
                <Text style={css.btnLogInText}> {'Continuar a Iniciar Sesión'} </Text>
              </TouchableOpacity>
            </View>

           
          </View>
        </View>
      </View>
    );
  
}

