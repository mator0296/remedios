import React, { Component } from 'react';
import {
	View,
	Text,
	Platform,
	Modal,
	TouchableOpacity,
	Linking,
	AsyncStorage,
	TextInput
} from 'react-native';
import Icon from '@expo/vector-icons/SimpleLineIcons'
import css from './styles'
import { Events, Config, Languages, Constants } from '@common';

export default class ConfirmModal extends Component {
	constructor(props) {
		
		super(props)
		this.state  = {
			visible:true,
			email:''
		}
		this.cancelar = this.cancelar.bind(this);
		this.singUp = this.singUp.bind(this);

	}

	cancelar() {
		this.setState({visible:false});
		this.props.cancel();
	}
	singUp() {
		if (!this.state.showError && this.state.email !== "") {
			this.setState({visible:false});
			this.props.btnSignUp();
		}
	}



		
	render() {
		return (
			<View>
				<Modal
					animationType="slide"
	                transparent={true}
	                visible={this.state.visible}
	                onRequestClose={() => {
	                    
	                }}>
					<View style={css.bodyModal}>
						<View style={css.contentModal}>

							<Text style={css.textTitle}>{'Confirmación'}</Text>
							<View style={css.divider}/>

							<Text style={css.textBody}>{`Hola ${this.props.name}, deseas registrarte con el siguiente correo ${this.props.email}`}</Text>
							<View style={css.textInputWrap}>
					          <Text style={css.textLabel}>{"Confirma tu correo"}</Text>
					          <TextInput
					            underlineColorAndroid="transparent"
					            placeholder={Languages.enterEmail}
					            style={css.textInput}
					            onChangeText={(text) => this.setState({ email: text , showError: text!==this.props.email})}
					          />
					          {this.state.showError && <Text style={css.textTitleError}>{'Correo no coincide'}</Text>}
					        </View>
							<View style={css.buttonContainer}>
								<TouchableOpacity style={css.button} onPress={this.cancelar}>
									<Text style={css.textButton}>{'Cancelar'}</Text>							
								</TouchableOpacity>
							
								<TouchableOpacity style={css.button} onPress={this.singUp}>
									<Text style={css.textButton}>{'Registrarme'}</Text>	
								</TouchableOpacity>
							</View>								
						</View>
					</View>
				</Modal>
			</View>
		);
	}
}
