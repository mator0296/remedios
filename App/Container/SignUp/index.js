/** @format */

import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, TextInput } from 'react-native';
import { Tools, Events, Languages, Config } from '@common';
import { Spinkit } from '@components';
import User from '@services/User';
import Icon from '@expo/vector-icons/FontAwesome';
import css from './styles';
import { Facebook, Google } from '@expo';
import { fetchUserData } from '@redux/actions';
import { connect } from 'react-redux';
import SuccessMessage from './successMessage';
import ShowConfirm from './confirm';

class SignUp extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      email: '',
      name: '',
      showSuccess:false,
    };
  }

  logInWithFacebook = async () => {
    this.setState({ appLoading: true });
    console.log(Config.Facebook.logInID)
    const result = await Facebook.logInWithReadPermissionsAsync(
      Config.Facebook.logInID,
      {
        permissions: ['public_profile', 'email'],
      }
    );
    console.log(result)
    let emailPermission = false;
    for (var i = result.permissions.length - 1; i >= 0; i--) {
      if (result.permissions[i] === 'email')
        emailPermission = true;
    }
    if (!emailPermission) {
       Events.loginShowError('Necesitamos su correo para el funcionamiento de la app!!');
       return;
    }

    if (result.type === 'success') {
      const token = result.token;
      User.loginFacebook(token.toString()).then(() => {
        Tools.refresh();
        // Events.loginShowInfo(Languages.loginSuccess);
        User.clearPosts();
        Events.closeUserModal();
        this.props.fetchUserData();
      });
    } else {
      this.setState({ appLoading: false });
    }
  };

  confirm= ()=>{
    if (
      this.state.email !== '' &&
      this.state.name !== ''
    ) { 
      this.setState({showConfirm:true})
    }
  }

  cancel= ()=>{
    this.setState({showConfirm:false})
  }

  btnSignUp = () => {
    if (
      this.state.email !== '' &&
      this.state.name !== ''
    ) {
      this.setState({
        loading: true,
        showConfirm:false,
      });


      User.create(
        this.state.email,
        this.state.name,
        () => {
          //Events.loginShowInfo(Languages.signupSuccess);
          this.setState({ loading: false, showSuccess:true });
          User.clearPosts();
          Tools.refresh();
          //Events.closeUserModal();
        },
        (error) => {
          this.setState({ loading: false });
          if (typeof error.msg !== 'undefined') {
           
              Events.loginShowError('Correo ya esta registrado');
            
            
          } else if (typeof error.error !== 'undefined') {
            Events.loginShowError('Correo ya esta registrado');
          }
          /*  if (error.message === 'Password should be at least 6 characters') {
              Events.loginShowError('La contraseña debe tener mínimo 6 caracteres');
            } else if (error.message === 'The email address is already in use by another account.') {
              Events.loginShowError('Correo ya registrado');
            }  else if (error.message === 'The email address is badly formatted.') {
              Events.loginShowError('Correo en formato incorrecto');
            }
            
          } else if (typeof error.error !== 'undefined') {
            Events.loginShowError(error.error);
          }*/
        },
        true
      );
    }
  };

  render() {
    if (this.state.showSuccess) 
      return <SuccessMessage goToLogin={this.props.changeRecevery} email={this.state.email}></SuccessMessage>
    return (
      <View style={css.wrap}>
        {this.state.showConfirm && <ShowConfirm name={this.state.name}  email={this.state.email} cancel={this.cancel} btnSignUp={this.btnSignUp}></ShowConfirm>}
        <View style={css.body}>
          <View style={css.wrapForm}>
            <View style={css.textInputWrap}>
              <Text style={css.textLabel}>{Languages.name}</Text>
              <TextInput
                placeholder={Languages.enterName}
                underlineColorAndroid="transparent"
                style={css.textInput}
                onChangeText={(text) => this.setState({ name: text })}
              />
            </View>

            <View style={css.textInputWrap}>
              <Text style={css.textLabel}>{Languages.email}</Text>
              <TextInput
                underlineColorAndroid="transparent"
                placeholder={Languages.enterEmail}
                style={css.textInput}
                onChangeText={(text) => this.setState({ email: text })}
              />
            </View>
            <View style={css.wrapButton}>
              {this.state.loading ? (
                <TouchableOpacity style={css.btnLogIn}>
                  <Spinkit size={20} type="FadingCircle" color="#FFFFFF" />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity style={css.btnLogIn} onPress={this.confirm}>
                  <Text style={css.btnLogInText}> {Languages.signup} </Text>
                </TouchableOpacity>
              )}
              <TouchableOpacity
                style={[css.btnLogIn, css.buttonFacebook]}
                onPress={this.logInWithFacebook}>
                
                    <Icon
                    name="facebook"
                    size={20}
                    color="#fff"
                    style={css.iconButton}
                  />
                  <Text style={[css.btnLogInLabel, {color:'white'}]}>Registrarse con Facebook</Text>
                

              </TouchableOpacity>
              <TouchableOpacity onPress={this.props.changeRecevery}>
                <Text style={{color: '#333', fontWeight: 'bold', fontSize: 14, marginBottom:'5%'}}>{'¿Ya eres miembro? '}<Text style={{color: 'blue', fontSize: 14, textAlign: 'right', textDecorationLine:'underline'}}>{'Inicia sesión'}</Text> </Text>
              </TouchableOpacity> 
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  null,
  { fetchUserData }
)(SignUp);
