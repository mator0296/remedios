/** @format */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, TextInput } from 'react-native';
import { Spinkit } from '@components';
import { Facebook, Google } from '@expo';
import { Events, Tools, Config, Languages } from '@common';
import User from '@services/User';
import Icon from '@expo/vector-icons/FontAwesome';
import { fetchUserData } from '@redux/actions';
import { connect } from 'react-redux';
import css from './style';

class SignIn extends PureComponent {
  static propTypes = {
    fetchUserData: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      appLoading: false,
      email: '',
      password: '',
    };
  }

  btnLogIn = () => {


    if (this.state.email !== '' && this.state.password !== '') {
     this.setState({ loading: true });
     
      User.login(
        this.state.email.trim(),
        this.state.password,
        () => {
          Events.loginShowInfo(Languages.loginSuccess);
          this.setState({ loading: false });
          User.clearPosts();
          Tools.refresh();
          this.props.fetchUserData();
          Events.closeUserModal();
        },
        (error) => {
          
          this.setState({ loading: false });
          Events.loginShowError('Correo o contraseña invalidas');
        }
      );
    }
  };

  signInWithGG = async () => {
    try {
      this.setState({ appLoading: true });

      const result = await Google.logInAsync({
        androidClientId: Config.Google.androidClientId,
        iosClientId: Config.Google.iosClientId,
        scopes: ['profile', 'email'],
      });

      if (result.type === 'success') {
        User.loginGoogle(result).then(() => {
          Events.closeUserModal();

          // console.log(result)

          // console.log(result)

          // Events.loginShowInfo(Languages.loginSuccess);
          // User.clearPosts();
          Tools.refresh();
          this.props.fetchUserData();
        });
      } else {
        return { cancelled: true, appLoading:false };
      }
    } catch (e) {
      return { error: true, cancelled:true };
    }
  };

  logInWithFacebook = async () => {
    //this.setState({ appLoading: true });
    const result = await Facebook.logInWithReadPermissionsAsync(
      Config.Facebook.logInID,
      {
        permissions: ['public_profile', 'email'],
      }
    );
    let emailPermission = false;
    for (var i = result.permissions.length - 1; i >= 0; i--) {
      if (result.permissions[i] === 'email')
        emailPermission = true;
    }
    if (!emailPermission) {
       Events.loginShowError('Necesitamos su correo para el funcionamiento de la app!!');
       return;
    }
    
    if (result.type === 'success') {
      const token = result.token;
      User.loginFacebook(token.toString()).then(() => {
        Tools.refresh();
        // Events.loginShowInfo(Languages.loginSuccess);
        User.clearPosts();
        Events.closeUserModal();
        this.props.fetchUserData();
      });
    } else {
      this.setState({ appLoading: false });
    }
    
  };

  render() {
    if (this.state.appLoading) {
      return <Spinkit size={30} type="FadingCircle" color="#FFFFFF" />;
    }

    return (
      <View style={css.wrap}>
        <View style={css.body}>
          <View style={css.wrapForm}>
            <View style={css.textInputWrap}>
              <Text style={css.textLabel}>{Languages.email}</Text>
              <TextInput
                placeholder={Languages.enterEmail}
                underlineColorAndroid="transparent"
                style={css.textInput}
                onChangeText={(text) => this.setState({ email: text })}
              />
            </View>

            <View style={css.textInputWrap}>
              <Text style={css.textLabel}>{Languages.passwordUp}</Text>
              <TextInput
                placeholder={Languages.enterPassword}
                underlineColorAndroid="transparent"
                style={css.textInput}
                secureTextEntry
                onChangeText={(text) => this.setState({ password: text })}
              />
            </View>
            <TouchableOpacity onPress={this.props.changeRecevery}>
              <Text style={{color: 'blue', fontSize: 16, textAlign: 'right', textDecorationLine:'underline'}}>{'¿Olvidaste tu Contraseña?'}</Text>
            </TouchableOpacity>
          </View>

          <View style={css.wrapButton}>
            {this.state.loading ? (
              <TouchableOpacity style={css.btnLogIn}>
                <Spinkit />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity style={css.btnLogIn} onPress={this.btnLogIn}>
                <Text style={css.btnLogInText}> {Languages.login} </Text>
              </TouchableOpacity>
            )}
            
            <Text style={{color: '#333', fontWeight: 'bold', fontSize: 14, marginBottom:'5%'}}>{'O inicia sesión con Facebook'} </Text>
            
            <TouchableOpacity
              style={[css.btnLogIn, css.buttonFacebook]}
              onPress={this.logInWithFacebook}>
              
                  <Icon
                  name="facebook"
                  size={20}
                  color="#fff"
                  style={css.iconButton}
                />
                <Text style={[css.btnLogInLabel, {color:'white'}]}>Iniciar Sesión con Facebook</Text>
              

            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.props.goToPage(1)}>
              <Text style={{color: '#333', fontWeight: 'bold', fontSize: 14, marginBottom:'5%'}}>{'¿Eres nuevo? '}<Text style={{color: 'blue', fontSize: 14, textAlign: 'right', textDecorationLine:'underline'}}>{'Regístrate'}</Text> </Text>
            </TouchableOpacity>  
            
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  null,
  { fetchUserData }
)(SignIn);
