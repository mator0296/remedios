import React, { Component } from 'react';
import {
	View,
	Text,
	Platform,
	Modal,
	TouchableOpacity,
	Linking,
	AsyncStorage
} from 'react-native';
import Icon from '@expo/vector-icons/SimpleLineIcons'
import css from './styles'
import { Events, Config, Languages, Constants } from '@common';

export default class ReviewModal extends Component {
	constructor(props) {
		
		super(props)
		this.state  = {
			visible:false,
		}

	}
	async componentDidMount() {
		const time = await AsyncStorage.getItem('time');
		if (time) {
			if (time === 'dontShow') return;
			if ((new Date().getTime() - Number(time))/(1000*60*60*24) >= Constants.daysMessageReview) 
				this.setState({visible:true})
			
		} else {
			
			await AsyncStorage.setItem('time', String(new Date().getTime()));
		}
		
	}

	goToContact = async () => {
		this.setState({visible:false})
	    /*this.props.navigation.navigate('customPage', {
	      id: Config.CustomPages.contact_id,
	      title: Languages.contact,
	      showMessage:true
	    })*/
	    Linking.openURL(Config.urlSupport)
	    await AsyncStorage.setItem('time', 'dontShow');
	}

	openLink = async () => {

		if (Platform.OS === 'ios') {
			Linking.openURL(Config.urlAppStore)
		} else {
			Linking.openURL(Config.urlPlayStore)
		}
		
		this.setState({visible:false})
		await AsyncStorage.setItem('time', 'dontShow');
	}

	render() {
		return (
			<View>
				<Modal
					animationType="slide"
	                transparent={true}
	                visible={this.state.visible}
	                onRequestClose={() => {
	                    
	                }}>
					<View style={css.bodyModal}>
						<View style={css.contentModal}>

							<Text style={css.textTitle}>{'Califica esta aplicación'}</Text>
							<View style={css.divider}/>

							<Text style={css.textBody}>{'Si a ti te gusta esta aplicación, por favor toma un momento para calificarla.\nNo tomara mucho mas de un minuto. Muchas gracias por tu apoyo!'}</Text>
							<View style={css.buttonContainer}>
								<TouchableOpacity style={css.button} onPress={this.goToContact}>
									<Text style={css.textButton}>{'No me gusta'}</Text>							
								</TouchableOpacity>
							
								<TouchableOpacity style={css.button} onPress={this.openLink}>
									<Text style={css.textButton}>{'Me gusta'}</Text>	
								</TouchableOpacity>
							</View>								
						</View>
					</View>
				</Modal>
			</View>
		);
	}
}
