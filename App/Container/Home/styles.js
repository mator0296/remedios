/** @format */

import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  scrollView: {
    marginTop: 40,
    paddingBottom: 40,
    backgroundColor: '#fff',
  },
  body: {
    flex: 1,
    backgroundColor: '#fff',
  },
  navbar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    backgroundColor: '#e00',
    borderBottomColor: '#dedede',
    borderBottomWidth: 0,
    height: 40,
    justifyContent: 'center',
  },
  bodyModal: {
    height:'100%',
    width:'100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentModal: {
    backgroundColor:'white',
    minHeight:300,
    width:'90%',
    borderRadius:20,
    borderColor:'#333',
    borderWidth:2,
    padding:'5%',
  },
  iconZoom: {
    position: 'absolute',
    right: 7,
    top: 10,
    backgroundColor: 'transparent',
    paddingTop: 4,
    paddingRight: 4,
    paddingBottom: 4,
    paddingLeft: 4,
    marginRight: 10,
    zIndex: 9999,
  },
  textClose: {
    color:'#333'
  },
  textTitle: {
    textAlign:'left',
    fontSize:22,
    fontWeight:'bold',
    color:'#333'
  },
  textBody:{
    textAlign:'left',
    fontSize:18,
    color:'#333'
  },
  divider:{
    backgroundColor:'#333',
    width:'100%',
    height: 2,
    marginBottom:'7%'
  },
  button: {
    flex:1,
    borderWidth:1,
    borderColor:'#eee',
    margin:10,
    backgroundColor: 'rgb(72,194,172)',
    borderRadius:20,
    padding:10,
  },
  textButton: {
    color:'white',
    textAlign:'center'
  },
  buttonContainer: {
    flexDirection:'row',
    width:'100%',
    position:'absolute',
    bottom:'10%',
    left:'6%',
    
  }
})
