/** @format */

import React, { PureComponent } from 'react'
import { View } from 'react-native'
import ScrollableTabView, {
  ScrollableTabBar,
} from 'react-native-scrollable-tab-view'
import DropdownAlert from 'react-native-dropdownalert'
import { SignIn, SignUp, RecoveryPassword } from '@container'
import { Color, Languages, Constants, Events } from '@common'
import styles from './style'

export default class LogIn extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      page:0
    };
  }
  componentWillMount() {
    Events.onLoginShowError(this.showError)
    Events.onLoginShowInfo(this.showSuccess)
  }

  showError = (message) => {
    this.dropdown && this.dropdown.alertWithType('error', 'Error', message)
  }

  showSuccess = (message) => {
    this.dropdown && this.dropdown.alertWithType('success', 'Éxito', message)
  }

  changeRecevery() {
    
    this.setState({showRecovery:!this.state.showRecovery, page:0})
  }

  goToPage(page) {
    this.setState({page})
   

  }

  goToLogin() {
    this.setState({page:0, showRecovery:false})
  }

  render() {
    
    return (
      <View style={styles.body}>
        <ScrollableTabView
          page={this.state.page}
          tabBarUnderlineStyle={styles.activeTab}
          tabBarActiveTextColor={Color.tabbarTint}
          tabBarInactiveTextColor="rgba(0, 0, 0, 0.4)"
          tabBarTextStyle={styles.textTab}
          animationEnabled={false}
          swipeEnabled={false}
          lazyLoad
          onChangeTab={(data)=>this.setState({page:data.i})}
          renderTabBar={() => (
            <ScrollableTabBar
              underlineHeight={0}
              style={{ borderBottomColor: 'transparent' }}
              tabsContainerStyle={{ paddingLeft: 0, paddingRight: 0 }}
              tabStyle={styles.tab}
            />
          )}
        >
          {Constants.RTL ? (
            <SignUp changeRecevery={this.goToLogin.bind(this)} goToPage={this.goToPage.bind(this)} tabLabel={Languages.signup} />
          ) : (
            this.state.showRecovery ? 
              <RecoveryPassword goToPage={this.goToPage.bind(this)} tabLabel={'Recuperar'} changeRecevery={this.changeRecevery.bind(this)} /> 
              : 
              <SignIn changeRecevery={this.changeRecevery.bind(this)}  goToPage={this.goToPage.bind(this)} tabLabel={Languages.login} />
          )}

          {Constants.RTL ? (
            this.state.showRecovery ? 
              <RecoveryPassword goToPage={this.goToPage.bind(this)} tabLabel={'Recuperar'} changeRecevery={this.changeRecevery.bind(this)} /> 
              : 
              <SignIn changeRecevery={this.changeRecevery.bind(this)}  goToPage={this.goToPage.bind(this)} tabLabel={Languages.login} />
          ) : (
            <SignUp  changeRecevery={this.goToLogin.bind(this)} goToPage={this.goToPage.bind(this)} tabLabel={Languages.signup} />
          )}
          
        </ScrollableTabView>

        <DropdownAlert ref={(ref) => (this.dropdown = ref)} />
      </View>
    )
  }
}
