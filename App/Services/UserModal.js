/** @format */

import { AsyncStorage } from 'react-native'
import { Constants, Config } from '@common'
import * as Analitys from '@common/Analitys'
import firebaseApp from '@services/Firebase'
import * as firebase from 'firebase'
import { remove, findIndex } from 'lodash'
import Api from './Api'

function UserModal() {
  if (!(this instanceof UserModal)) {
    return new UserModal()
  }
}

/**
 * Check the user login data is existing on the app
 * @returns {boolean}
 */
UserModal.prototype.isLogedIn = async function() {
  try {
    const value = await AsyncStorage.getItem(Constants.Key.user)

    return value
  } catch (error) {

  }
}

/**
 * create account with firebase
 * @param email
 * @param password
 * @param callBackFunc
 * @param onError
 */
UserModal.prototype.createWithFirebase = async function(
  email,
  password,
  callBackFunc,
  onError
) {
  
    return firebaseApp
      .auth()
      .createUserWithEmailAndPassword(email, password)
  
}

/**
 * Create the user login
 * @param email
 * @param password
 * @param callBackFunc
 * @param onError
 * @param isExisted
 */
// #1
UserModal.prototype.create = async function(
  email,
  name,
  callBackFunc,
  onError,
  isExisted
) {
  try {
    // existed on wordpress create with firebase
    if (Constants.firebaseEnable) {
      Api.emailExits(email).then( async (userData)=>{
        console.log(userData)
        if (userData.status === 'ok') {
           const wordpressRegisterd = await Api.register(email, name);
            if (
              typeof wordpressRegisterd.status !== 'undefined' &&
              wordpressRegisterd.status === 'ok'
            ) {
              
              ///this.saveUser(userData, email)
              if (typeof callBackFunc === 'function') {
                callBackFunc()
              }
              
            } else {
              callBackFunc()
            }
          } else {
            onError(userData)
          }
       
      })
      .catch((error) => {
        console.log(error)
        if (typeof onError === 'function') {
          onError(error)
        }
      })
    }
    // register on Wordpress site

    
  } catch (error) {
    console.log(error)
  }
}
/**
 * login on both Wordpress and Firebase
 * @param email
 * @param password
 * @param callBackFunc
 */
// #2
UserModal.prototype.login = async function(
  email,
  password,
  callBackFunc,
  onError
) {
  try {
    const wordpressLoggined = await Api.generateAuthCookie(email, password)
    if (
      typeof wordpressLoggined.status !== 'undefined' &&
      wordpressLoggined.status == 'ok'
    ) {
      Api.lastLogin(wordpressLoggined.cookie).then(data=>console.log('respone',data)).catch(error=>console.log('error', error))
      const userData = wordpressLoggined

      this.saveUser(userData.user, email, wordpressLoggined.cookie)
      // if enable user also save to Firebase
      if (Constants.firebaseEnable){
        firebaseApp
          .auth()
          .signInWithEmailAndPassword(email, password + 'password')
          .then(() => {
            if (typeof callBackFunc === 'function') {
              callBackFunc()
              Analitys.setUser(userData.user.id)
            }
            return true
          })
          .catch((error) => {
            // if use not found on firebase, just create it for saving app data
            
            if (error.code === 'auth/user-not-found') {
              firebaseApp
                .auth()
                .createUserWithEmailAndPassword(email, password + 'password')
                .then(() => {
                  if (typeof callBackFunc === 'function') {
                    callBackFunc()
                    Analitys.setUser(userData.user.id)
                  }
                  return true
                })
            } else if (typeof onError === 'function') {
              callBackFunc()
              Analitys.setUser(userData.user.id)
            }
          })
      } else if (typeof callBackFunc === 'function') {
        callBackFunc()
        Analitys.setUser(userData.user.id)
      }
    } else if (typeof onError === 'function') {
      onError({ message: wordpressLoggined.error })
    }
  } catch (error) {
    console.log(error)
  }
}

UserModal.prototype.recoveryPassword = async function(
  email,
  callBackFunc,
  onError
) {
  try {
    const wordpressLoggined = await Api.recoveryPassword(email)
    
    if (
      typeof wordpressLoggined.status !== 'undefined' &&
      wordpressLoggined.status == 'ok'
    ) {
      // if enable user also save to Firebase
      if (typeof callBackFunc === 'function') {
        callBackFunc()
        
      }
    } else if (typeof onError === 'function') {
      onError({ message: wordpressLoggined.error })
    }
    callBackFunc()
  } catch (error) {
    console.log(error)
  }
}

UserModal.prototype.loginFacebook = async function(accessToken) {
  try {
    const auth = firebaseApp.auth()
    const credential = firebase.auth.FacebookAuthProvider.credential(
      accessToken
    )

    const data = await auth.signInAndRetrieveDataWithCredential(credential)

    let userEmail = data.email

    if (userEmail == null) {
      userEmail = `${data.uid}@facebook.com`
    }

    const dataFacebook = await Api.generate_auth_facebook(accessToken)
    Analitys.setUser(dataFacebook.wp_user_id)
    Api.lastLogin(dataFacebook.cookie).then(data=>console.log('respone',data))
    
    
    return this.saveUser(data, userEmail, dataFacebook.cookie)
  } catch (error) {
    console.log(error)
  }
}

UserModal.prototype.loginGoogle = async function(input) {
  const auth = firebaseApp.auth()
  const credential = firebase.auth.GoogleAuthProvider.credential(input.idToken)
  const data = await auth
    .signInAndRetrieveDataWithCredential(credential)
    .catch((err) => console.log(err))
  return this.saveUser(data, input.user.email)
}

/**
 * Save user data to offline
 * @param userData
 * @param email
 */
UserModal.prototype.saveUser = async function(userData = {}, email, cookie) {
  console.log("save")
  try {
    await AsyncStorage.removeItem(Constants.Key.user)
    await AsyncStorage.setItem(Constants.Key.user, JSON.stringify(userData))
    await AsyncStorage.setItem(Constants.Key.email, email)
    await AsyncStorage.setItem(Constants.Key.cookie, cookie)
  } catch (error) {

  }
}

/**
 * get read later user
 */
UserModal.prototype.getUser = async function() {
  try {
    let userData = await AsyncStorage.getItem(Constants.Key.user)
   
    if (userData != null) {
      userData = JSON.parse(userData); 
      return userData
    }
  } catch (error) {}
}

UserModal.prototype.getPosts = async function() {
  const userPosts = await AsyncStorage.getItem(Constants.Key.posts)

  let postData = []
  if (userPosts != null) {
    postData = JSON.parse(userPosts)
  }

  // if the offline data is null let check the online and sync back to the app
  if (postData.length == 0) {
    postData = await this.getFirebasePost()
    AsyncStorage.setItem(Constants.Key.posts, JSON.stringify(postData))
  }

  return postData
}

/**
 * Get the data from firebase
 * @returns {*|string|string}
 */
UserModal.prototype.getFirebasePost = async function() {
  const userData = await this.getUser()
  let postData = []

  if (typeof userData !== 'undefined' && typeof userData.uid !== 'undefined') {
    const data = await firebaseApp
      .database()
      .ref(`/${Config.Firebase.readlaterTable}/${userData.uid}`)
      .once('value')

    if (data.val() != null) {
      postData = data.val().post
    }
  } else {

  }

  return postData
}

/**
 * Add the post to firebase
 * @param post
 */
UserModal.prototype.firebaseSync = async function(postData) {
  const userData = await this.getUser()

  if (typeof userData !== 'undefined' && typeof userData.uid !== 'undefined') {
    firebaseApp
      .database()
      .ref(Config.Firebase.readlaterTable)
      .child(`${userData.uid}/post`)
      .set(postData)
  }
}

/**
 * Save read later post and sync to firebase
 * @param post
 */
UserModal.prototype.savePost = async function(post, fnc) {
  if (typeof post === 'undefined' || post == null) {
    return
  }

  const userPosts = await AsyncStorage.getItem(Constants.Key.posts)
  let postData = []
  if (userPosts != null) {
    postData = JSON.parse(userPosts)
  }

  const indexPost = findIndex(postData, (data) => {
    return data.id == post.id
  })

  // not in the read later array yet
  if (indexPost == -1) {
    postData.push(post)

    // save to storage local
    await AsyncStorage.setItem(Constants.Key.posts, JSON.stringify(postData))

    if (typeof fnc === 'function') {
      fnc()
    }

    // sync to firebase
    this.firebaseSync(postData)
  }
}

/**
 * remove read later post
 * @param post
 */
UserModal.prototype.removePost = async function(post) {
  const userPosts = await AsyncStorage.getItem(Constants.Key.posts)
  let postData = []
  if (userPosts != null) {
    postData = JSON.parse(userPosts)
  }
  const newPostData = remove(postData, (data) => {
    return data.id != post.id
  })

  AsyncStorage.setItem(Constants.Key.posts, JSON.stringify(newPostData))

  // sync to firebase
  this.firebaseSync(newPostData)
}

/**
 * Remove all read later post
 */
UserModal.prototype.clearPosts = function(isSync) {
  // should remove online also
  if (typeof isSync !== 'undefined') {
    this.firebaseSync(null)
  }
  AsyncStorage.getItem(Constants.Key.posts).then((data) => {
    if (data != null) {
      return AsyncStorage.multiRemove([Constants.Key.posts], (err) => {})
    }
  })
}

/**
 * Logout and delete all offline data
 */
UserModal.prototype.logOut = function() {
  AsyncStorage.getItem(Constants.Key.user).then((data) => {
    if (data != null) {
      return AsyncStorage.multiRemove([Constants.Key.user], (err) => {})
    }
  })
}

export default UserModal
