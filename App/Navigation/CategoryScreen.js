/** @format */

import React, { Component } from 'react'
import { Style } from '@common'
import { Category } from '@container'
import Icons from './Icons'

export default class CategoryScreen extends Component {
  static navigationOptions = {
    headerLeft: Icons.Home(),
    header: null,
    headerStyle: Style.toolbar,
  }

  render() {
    const { navigate } = this.props.navigation
    return (
      <Category
        onViewPost={(item, index, parentPosts) =>
          navigate('postDetail', { post: item, index, parentPosts, backToRoute: 'category' })
        }
        onViewCategory={(config) => navigate({routeName: 'PostListScreen', params: config})}
      />
    )
  }
}
