import React, { Component } from 'react'
import {
  fetchCategories,
  setActiveLayout,
  setActiveCategory,
} from '@redux/actions'
import { flatten } from 'lodash'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Constants } from '@common'
import { PostList } from '@components'

class PostListScreenJuicyn extends Component {
  constructor(props) {
    super(props)
    this.showCategory();
  }
  showCategory = (category) => {
    const { setActiveCategory, onViewCategory } = this.props
    setActiveCategory(171)
    
  }
  static navigationOptions = ({ navigation }) => ({
    title: 'Juicy',
    header: null,
  })

  static propTypes = {
    navigation: PropTypes.object,
  }

  render() {
    
    const { navigate, goBack, state } = this.props.navigation;
    const params = state.params

    return (
      <PostList
        config={{ name: 'Juicing', category: 171 }}
        goBack={() => goBack()}
        noBack={true}
        layout={Constants.Layout.twoColumn}
        onViewPost={(item, index, parentPosts) =>
          navigate('postDetail', {
            post: item,
            index,
            parentPosts,
            backToRoute: 'category'
          })
        }
      />
    )
  }
}

const mapStateToProps = (state) => {
  const selectedCategory = state.categories.selectedCategory
  const categories = flatten(state.categories.list)
  const selectedLayout = state.categories.selectedLayout
  return { categories, selectedCategory, selectedLayout }
}
export default connect(mapStateToProps, {
  fetchCategories,
  setActiveLayout,
  setActiveCategory,
})(PostListScreenJuicyn)
