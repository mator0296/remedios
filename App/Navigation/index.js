/** @format */

import React from 'react'
import {
  createBottomTabNavigator,
  createStackNavigator,
} from 'react-navigation'
import { Color, Images } from '@common'
import { TabBar, TabBarIcon } from '@components'
import Entypo from 'react-native-vector-icons/Entypo'
import PostDetailScreen from './PostDetailScreen'
import HomeScreen from './HomeScreen'
import SettingScreen from './SettingScreen'
import CategoryScreen from './CategoryScreen'
import CustomPageScreen from './CustomPageScreen'
import PhotoScreen from './PhotoScreen'
import VideoScreen from './VideoScreen'
import ReadLaterScreen from './ReadLaterScreen'
import PostListScreen from './PostListScreen'
import HorizontalScreen from './HorizontalScreen'
import SearchScreen from './SearchScreen'
import JuicyScreen from './JuicyScreen'

const categoryStack = createStackNavigator(
  {
    category: { screen: CategoryScreen },
    PostListScreen: { screen: PostListScreen },
  },
  {
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <TabBarIcon icon={Images.icons.category} tintColor={tintColor} />
      ),
      headerTintColor: '#333',
    },
  }
)

const newsStack = createStackNavigator(
  {
    home: { screen: HomeScreen },
    PostListScreen: { screen: PostListScreen },
    HorizontalScreen: { screen: HorizontalScreen },
  },
  {
    header: null,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <TabBarIcon icon={Images.icons.news} tintColor={tintColor} />
      ),
    },
  }
)

const videoStack = createStackNavigator(
  {
    video: { screen: VideoScreen },
  },
  {
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <TabBarIcon icon={Images.icons.video} tintColor={tintColor} />
      ),
    },
  }
)

const searchStack = createStackNavigator(
  {
    search: { screen: SearchScreen },
    searchPostDetail: { screen: PostDetailScreen },
  },
  {
    header: null,
  }
)

const JuicyStack = createStackNavigator({
  Juicy: {
    screen: JuicyScreen
  },
  PostListScreenJuicy: { screen: PostListScreen }

},{
    header:null
  })


export default createBottomTabNavigator(
  {
    home: {
      screen: newsStack,
      navigationOptions: {
        header: null,
        tabBarIcon: ({ tintColor }) => (
          <TabBarIcon icon={Images.icons.news} tintColor={tintColor} />
        ),
      },
    },

    category: {
      screen: categoryStack,
      navigationOptions: {
        header: null,
        tabBarIcon: ({ tintColor }) => (
          <TabBarIcon icon={Images.icons.category} tintColor={tintColor} />
        ),
      },
    },

    search: {
      screen: searchStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <TabBarIcon icon={Images.icons.search} tintColor={tintColor} />
        ),
      },
    },

    video: {
      screen: videoStack,
      navigationOptions: {
        header: null,
        tabBarIcon: ({ tintColor }) => (
          <TabBarIcon icon={Images.icons.video} tintColor={tintColor} />
        ),
      },
    },
    photo: { screen: JuicyStack,
      navigationOptions: {
        header: null,
        tabBarIcon: ({ tintColor }) => (
          <Entypo name='cup' size={22} color={tintColor}></Entypo>
        ),
      },
    },
    readlater: { screen: ReadLaterScreen },
    postDetail: { screen: PostDetailScreen },
    customPage: { screen: CustomPageScreen },
    setting: { screen: SettingScreen },
  },
  {
    tabBarComponent: TabBar,
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    animationEnabled: false,
    tabBarOptions: {
      showIcon: true,
      showLabel: false,
      activeTintColor: Color.tabbarTint,
      inactiveTintColor: Color.tabbarColor
    },
    lazy: true
  }
)
