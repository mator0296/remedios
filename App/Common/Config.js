/** @format */

import Constants from './Constants'

export default {
  /**
   * The detail document from: https://beonews.inspireui.com
   * Step 1: Moved to AppConfig.json
   */

  Banner: {
    visible: true,
    sticky: true,
    tag: [],
    categories: [],
  },
  apiKey:'5c1934f5eb01b',
  GA_TRACKER_ID: 'UA-130120262-1',
  GA_TRACKER_INTERVAL: 1,
  urlPlayStore:'https://play.google.com/store',
  urlAppStore:'https://play.google.com/store',
  urlSupport:'https://remediosnaturales.com/me',
  /**
   * Advance config
   * CategoryVideo: config the category id for video page
   *
   * */
  // Category video id from the menu
  CategoryVideo: 170,

  imageCategories: {
    drink: require('@images/category/cate1.jpg'),
    remedios: require('@images/category/cate3.jpg'),
    juicing: require('@images/category/cate4.jpg'),
    smoothies: require('@images/category/cate5.jpg'),
    'bajar-de-peso': require('@images/category/cate6.jpg'),
    medicinales: require('@images/category/cate7.jpg'),
    'condiciones-de-salud': require('@images/category/cate8.jpg'),
    'quick-recipes': require('@images/category/cate9.jpg'),
    deserts: require('@images/category/cate10.jpg'),
    salud: require('@images/category/cate11.jpg'),
    video: require('@images/category/cate16.jpg'),
    featured: require('@images/category/cate12.jpg'),
    all: require('@images/category/cate14.jpg'),
  },


  // Custom page from left menu side
  CustomPages: {
    contact_id: 2398,
    aboutus_id: 2396,
  },

  // config for Firebase, use to sync user data across device and favorite post
  Firebase: {
    apiKey: "AIzaSyC4okQhO_uswBht-0nK1Rsn1iw23Wa1qc8",
    authDomain: "remediosnaturales-73411.firebaseapp.com",
    databaseURL: "https://remediosnaturales-73411.firebaseio.com",
    projectId: "remediosnaturales-73411",
    storageBucket: "remediosnaturales-73411.appspot.com",
    messagingSenderId: "533766851138",
    readlaterTable: 'list_readlater'
  },
  // config for log in by Facebook
  Facebook: {
    showAds: true,
    adPlacementID: '382126225663914_382127032330500',
    logInID: '723335768018949',
    sizeAds: 'standard', // standard, large
  },

  // config for log in by Google
  // Google: {
  //   analyticId: 'UA-90561349-1',
  //   androidClientId:
  //     '338838704385-1om86241pq2qpg4qi677jb1ndo5jqfh2.apps.googleusercontent.com',
  //   iosClientId:
  //     '338838704385-1om86241pq2qpg4qi677jb1ndo5jqfh2.apps.googleusercontent.com',
  // },


  // The advance layout
  AdvanceLayout: [
    Constants.Layout.threeColumn,
    Constants.Layout.threeColumn,
    Constants.Layout.threeColumn,
    Constants.Layout.list,
    Constants.Layout.list,
    Constants.Layout.list,
    Constants.Layout.list,
    Constants.Layout.card,
    Constants.Layout.column,
    Constants.Layout.column,
  ],
  
  // config for log in by Admob
  AdMob: {
    visible: false,
    deviceID: 'pub-2101182411274198',
    unitID: 'ca-app-pub-2101182411274198/9145196933',
    unitInterstitial: 'ca-app-pub-2101182411274198/7326078867',
    isShowInterstital: false,
  },

  // tab animate
  tabBarAnimate: Constants.Animate.zoomIn,

  // config default for left menu
  LeftMenuStyle: Constants.LeftMenu.overlay,

  notification: {
    AppId: '85cbc2b5-4e0d-4214-9653-8054d06f4256',
    NewAppId: '88b0e176-5756-47b7-b061-aacea262421d',
  },

  OneSignal: {
    appId: '85cbc2b5-4e0d-4214-9653-8054d06f4256'
  },

  // update 18 May
  // showLayoutButton: Ability to show/hide the switch layout button at homescreen
  // homeLayout: default homeLayout UI: Constants.Layout.mansory, Constants.Layout.horizontal
  // showSwitchCategory: show the switch button on categories page
  showLayoutButton: true,
  homeLayout: Constants.Layout.horizontal,
  showSwitchCategory: false,

  EnableSanitizeHtml: true,
  RequiredLogin: true,
  showAppIntro: true,
  intro: [
    {
      title: 'Bienvenido a Remedios Naturales',
      description: 'Tenemos todo lo que necesitas para mejorar tu salud',
      backgroundColor: '#D5E8ED',
      source: require('@images/lottie/loading.json'),
      button: "SIGUIENTE"
    }, {
      title: 'Fácil de usar',
      description: 'Una aplicación diseñada pensando en ti, para que puedas navegar entre pantallas sin problemas',
      backgroundColor: '#FFF',
      source: require('@images/lottie/check_animation.json'),
      button: "SIGUIENTE"
    }, {
      title: 'Notificaciones inteligentes',
      description: 'Para que no te pierdas las actualizaciones del contenido que te gusta',
      backgroundColor: '#D6D7DD',
      source: require('@images/lottie/notification.json'),
      button: "FINALIZAR"
    }
  ],
}
