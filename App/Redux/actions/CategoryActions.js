/** @format */

import wp from '@services/WPAPI'
import { Constants } from '@common'
import {
  CATEGORY_FETCH_SUCCESS,
  CATEGORY_ON_SELECT,
  CATEGORY_SELECT_LAYOUT,
} from '@redux/types'
import fetch from './fetch'

export const fetchCategories = () => {
  const api = wp
    .categories()
    .param('exclude', Constants.excludeCategories)
    .perPage(50)
  return (dispatch) => fetch(dispatch, api, CATEGORY_FETCH_SUCCESS)
}

export const setActiveCategory = (id) => {
  return (dispatch) => {
    dispatch({ type: CATEGORY_ON_SELECT, payload: id })
  }
}

export const setActiveLayout = (id) => {
  return (dispatch) => {
    dispatch({ type: CATEGORY_SELECT_LAYOUT, payload: id })
  }
}
