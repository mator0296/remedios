/** @format */

import wp from '@services/WPAPI'
import { Constants, Config } from '@common'
import User from '@services/User'
import fetch from './fetch'

import {
  POST_FETCH_SUCCESS,
  STICKY_FETCH_SUCCESS,
  FETCH_PENDING,
  VIDEO_FETCH_SUCCESS,
  PHOTO_FETCH_SUCCESS,
  POST_RELATED_FETCH_SUCCESS,
  POST_CHANGE_LAYOUT_SUCCESS,
  POST_FETCH_MORE,
  VIDEO_FETCH_MORE,
  PHOTO_FETCH_MORE,
  FETCH_POST_BOOKMARK_SUCCESS,
  SEARCH_POSTS_PENDING,
  SEARCH_POSTS_SUCCESS,
  SEARCH_POSTS_FAIL,
  POST_TAG_FETCH_MORE,
  POST_TAG_FETCH_SUCCESS,
  POST_ID_FETCH_SUCCESS,
} from '@redux/types'

export const fetchPosts = (
  page = 1,
  tags = null,
  categories = null,
  sticky = false
) => {
  const limit = Constants.PagingLimit

  
  let api = wp
    .posts()
    .tags(tags)
    .categories(categories)
    .excludeCategories(Constants.excludeCategories)
    .perPage(limit)
    .page(page)
    .sticky(sticky)

  return (dispatch) => {
    dispatch({ type: FETCH_PENDING })

    if (page == 1) {
      return fetch(dispatch, api, POST_FETCH_SUCCESS)
    }
    return fetch(dispatch, api, POST_FETCH_MORE, { tag: tags })
  }
}

export const fetchStickyPost = () => {
  const { sticky, tag, category } = Config.Banner

  const api = wp
    .posts()
    .sticky(sticky)
    .tags(tag)
    .categories(category)

  return (dispatch) => fetch(dispatch, api, STICKY_FETCH_SUCCESS)
}

export const fetchVideos = (page) => {
  const limit = Constants.PagingLimit

  const api = wp
    .posts()
    .categories([Config.CategoryVideo])
    .perPage(limit)
    .page(page)

  return (dispatch) => {
    dispatch({ type: FETCH_PENDING })

    if (page == 1) {
      return fetch(dispatch, api, VIDEO_FETCH_SUCCESS)
    }
    return fetch(dispatch, api, VIDEO_FETCH_MORE)
  }
}

export const fetchPhotos = (page) => {
  const limit = Constants.PagingLimit
  const api = wp
    .posts()
    .perPage(limit)
    .page(page)

  return (dispatch) => {
    dispatch({ type: FETCH_PENDING })

    if (page == 1) {
      return fetch(dispatch, api, PHOTO_FETCH_SUCCESS)
    }
    return fetch(dispatch, api, PHOTO_FETCH_MORE)
  }
}

export const fetchPostsRelated = (page, categoryId, postCurrent) => {
  const api = wp
    .posts()
    .categories([categoryId])
    .exclude(postCurrent.id)
    .before(new Date(postCurrent.date))
    .page(page)
  return (dispatch) =>
    fetch(dispatch, api, POST_RELATED_FETCH_SUCCESS, { postId: postCurrent.id })
}

export const fetchPostsBookmark = () => {
  return (dispatch) => {
    User.getPosts().then((data) =>
      dispatch({ type: FETCH_POST_BOOKMARK_SUCCESS, payload: data })
    )
  }
}

export const changeLayout = (layout = Constants.Layout.default) => {
  return (dispatch) => {
    dispatch({ type: POST_CHANGE_LAYOUT_SUCCESS, layout: layout })
  }
}

export const searchPosts = (searchText) => {
  const api = wp.posts().search(searchText)

  return (dispatch) => fetch(dispatch, api, SEARCH_POSTS_SUCCESS)
}

export const fetchPostsByTag = (
  page = 1,
  tags = null,
  categories = null,
  index = 0
) => {
  const limit = Constants.PagingLimit

  let api = wp
    .posts()
    .tags(tags)
    .categories(categories)
    .excludeCategories(Constants.excludeCategories)
    .perPage(limit)
    .page(page)
    .sticky(false)

  return (dispatch) => {
    dispatch({ type: FETCH_PENDING })
    if (page == 1) {
      return fetch(dispatch, api, POST_TAG_FETCH_SUCCESS, { index: index })
    }
    return fetch(dispatch, api, POST_TAG_FETCH_MORE, { index: index })
  }
}

export const fetchPostById = (id) => {
  let api = wp
    .posts()
    .id(id)
    .sticky(false)

  return (dispatch) => {
    dispatch({ type: FETCH_PENDING })

    return fetch(dispatch, api, POST_ID_FETCH_SUCCESS)
  }
}
