
import React, { PureComponent } from 'react'
import {
  AdIconView,
  MediaView,
  AdChoicesView,
  TriggerableView,
  withNativeAd,
  AdSettings
} from 'react-native-fbads';
import { Text, View } from 'react-native';
import { Constants } from '@common'


AdSettings.addTestDevice(AdSettings.currentDeviceHash);

class AdComponent extends React.Component {
  constructor(props) {
    super(props);
    
  }
  render() {
    return (
      <View style={{flex:1, flexDirection:'row', minHeight:100, borderBottomWidth:1, borderColor:'#eee', borderStyle:'solid'}} >
        <View style={{flex:0.35}}>
          <MediaView style={{ width: '100%', height: '100%' }} />
          
        </View>
        <View style={{flex:0.75, padding:10}}>
          <AdChoicesView style={{ position: 'absolute', right: 10, bottom: 10, height:10, width:10 }} />
          <TriggerableView>

            <Text style={{color:'black', color: '#333',fontSize: 20, marginBottom:10}}>{this.props.nativeAd.headline + '\n'}</Text>
            <Text style={{color:'black', color: '#999',fontSize: 12}}>{this.props.nativeAd.linkDescription}</Text>
          </TriggerableView>
        </View>
        
        
      </View>
    );
  }
}

export default withNativeAd(AdComponent);