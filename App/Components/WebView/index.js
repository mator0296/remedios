/** @format */

import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Image,
  Dimensions,
  Linking,
  WebBrowser,
} from 'react-native'
import HTML from 'react-native-render-html'
import { Tools, Constants, error, warn, Config } from '@common'
import sanitizeHtml from 'sanitize-html'

const { height: PageHeight, width: PageWidth } = Dimensions.get('window')
import { WebView } from "react-native-webview";

export default class Index extends PureComponent {
  static propTypes = {
    html: PropTypes.any,
  }

  constructor(props) {
    super(props)
    this.state = { fontSize: Constants.fontText.size }
  }

  async componentWillMount() {
    const fontSize = await Tools.getFontSizePostDetail()

    /*this.setState({
      fontSize,
    })*/
  }

  onLinkPress = (url) => {
    if (typeof WebBrowser !== 'undefined') {
      WebBrowser.openBrowserAsync(url)
    } else {
      Linking.canOpenURL(url)
        .then((supported) => {
          if (!supported) {
          } else {
            return Linking.openURL(url)
          }
        })
        .catch((err) => error('An error occurred', err))
    }
  }

  render() {
    const htmlContent = sanitizeHtml(this.props.html, {
      allowedTags: [ 'html','b', 'p', 'i', 'img', 'em', 'strong', 'a', 'html', 'h3', 'ul', 'li', 'h2', 'h1', 'span' ,'div','ol','article','aside','button','option','input','table','tr','th'],
      allowedAttributes: {
        'a': [ 'href' ],
        'img' : ['src', 'alt', 'width', 'height']
      },
      allowedIframeHostnames: ['www.youtube.com']
    })
    const fontSize = this.state.fontSize
      ? this.state.fontSize
      : Constants.fontText.size

    console.log(fontSize)

    const tagsStyles = {
      a: { color: 'blue', fontSize },
      strong: { color: '#333', fontSize, fontWeight: '700' },
      p: { color: '#333', marginBottom: 5, fontSize, lineHeight: 24, marginBottom: 30},
      em: { fontStyle: 'italic', fontSize },
      video: { marginBottom: 5 },
      img: { resizeMode: 'cover' },
      html: {marginTop:20},
      ul: { color: '#333' },
      ol: { color: '#333' },
      li: { color: '#333', fontSize },
      h1: { fontSize: 35, lineHeight: 40, paddingLeft: 20, paddingRigth: 20, marginBottom:30},
      h2: {color: '#799908',fontSize : 30,marginBottom: 30,'fontWeight': 'bold'},
      h3 : {fontSize : fontSize + 5 , color: '#28384d', marginBottom: 30,fontWeight: 'bold'}
    }
    
    const renderers = {
      img: (htmlAttribs, children, convertedCSSStyles, passProps) => {
        const { src, width, height } = htmlAttribs
        if (!src) {
          return false
        }
        const newWidth = Dimensions.get('window').width - 20
        const newHeight = height * newWidth / width
        return (
          <Image
            key={passProps.key}
            source={{ uri: src }}
            style={{
              width: newWidth,
              height: newHeight,
              resizeMode: 'contain',
            }}
          />
        )
      },
      iframe: (htmlAttribs, children, convertedCSSStyles, passProps) => {
        if (htmlAttribs.src) {
          const newWidth = PageWidth
          const width = htmlAttribs.width
          const height = htmlAttribs.height
          const newHeight = height > 0 ? height * newWidth / width : width * 0.7
          const url = htmlAttribs.src

          return (
            <WebView
              key={`webview-${passProps.key}`}
              source={{ uri: url }}
              allowsInlineMediaPlayback
              mediaPlaybackRequiresUserAction={false}
              javaScriptEnabled
              scrollEnabled={false}
              style={{
                width: PageWidth,
                left: -12,
                height: newHeight + 15,
              }}
            />
          )
        }
      },
    }

    // warn(['content:', htmlContent])
    console.log(htmlContent)
    console.log(tagsStyles)
    return (
      <View style={{ padding: 12 }}>
        <HTML
          html={Constants.RTL ? `<div style="text-align: left;margin-bottom:30px;">${this.props.html}</div>` : this.props.html}                                                                                                                                                                                                                                                                                                 
          ignoredStyles={['font-family']}
          renderers={renderers}
          imagesMaxWidth={PageWidth}
          tagsStyles={tagsStyles}
          onLinkPress={(evt, href) => this.onLinkPress(href)}
          staticContentMaxWidth={PageWidth}
        />
      </View>
    )
  }
}
