import React, {Component} from "react";
import {Text, Image, Dimensions, Linking, TouchableOpacity} from "react-native";
import HTML from "react-native-render-html";
import {HTMLStyles} from "@custom/react-native-fence-html";
import {Tools, Constants, Events} from "@common";
const {width, height} = Dimensions.get("window");

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fontSize: Constants.fontText.size
    };

    Tools.getFontSizePostDetail().then((data) => {
      this.setState({fontSize: data})
    })
  }

  externalLink(url) {
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
      }
      else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }

  openModal(imagesource) {
    Events.openPhotoClick({image: imagesource});
  }

  render() {
    // const htmlContent = striptags(this.props.html, ['img', 'a', 'span', 'br', 'p', 'ul', 'ol', 'li', 'h1', 'h2', 'h3', 'h4']);
    const htmlContent = sanitizeHtml(this.props.html, {
      allowedTags: [ 'html','b', 'p', 'i', 'img', 'em', 'strong', 'a', 'html', 'h3', 'ul', 'li', 'h2', 'h1', 'span' ,'div','ol','article','aside','button','option','input','table','tr','th'],
      allowedAttributes: {
        'a': [ 'href' ],
        'img' : ['src', 'alt', 'width', 'height']
      },
      allowedIframeHostnames: ['www.youtube.com']
    })
    const fontSize = this.state.fontSize
      ? this.state.fontSize
      : Constants.fontText.size

    const tagsStyles = {
      a: { color: 'blue', fontSize },
      strong: { color: '#333', fontSize, fontWeight: '700' },
      p: { color: '#333', marginBottom: 5, fontSize, lineHeight: 24, marginBottom: 30},
      em: { fontStyle: 'italic', fontSize },
      video: { marginBottom: 5 },
      img: { resizeMode: 'cover' },
      html: {marginTop:20},
      ul: { color: '#333' },
      ol: { color: '#333' },
      li: { color: '#333', fontSize },
      h1: { fontSize: 35, lineHeight: 40, paddingLeft: 20, paddingRigth: 20, marginBottom:30},
      h2: {color: '#799908',fontSize : 30, borderTop: '2px dashed #666666',marginBottom: 30,textTransform: 'none','font-weight': 'bold'},
      h3 : {fontSize : fontSize + 5 , color: '#28384d', marginBottom: 30,textTransform: 'none',fontWeight: 'bold'}
    }

    const renderers = {
      img: (htmlAttribs, children, passProps) => {
        if (htmlAttribs.width == "120") {
          const imageSource = htmlAttribs.src; // .replace('/thumbs/thumbs_', '/');
          return (
            <TouchableOpacity style={{width: width / 2 - 80, marginBottom: 8, height: width / 2 - 80}}
                              onPress={this.openModal.bind(this, imageSource)}>
              <Image
                source={{uri: imageSource, width: width / 2 - 80, height: width / 2 - 80}}
                style={passProps.htmlStyles.img}
                {...passProps} />
            </TouchableOpacity>
          )
        }

        return (
          <Image
            source={{uri: htmlAttribs.src, resizeMode: 'contain', width: width - 10, height: width / 2 + 50}}
            style={passProps.htmlStyles.img}
            {...passProps} />)

      },
      a: (htmlAttribs, children, passProps) => {
        const style = []
          .concat(
            passProps.htmlStyles ? passProps.htmlStyles.a : undefined,
            htmlAttribs.style ? HTMLStyles.cssStringToRNStyle(htmlAttribs.style, HTMLStyles.STYLESETS.TEXT) : undefined
          ).filter((s) => s !== undefined)

        if (passProps.parentIsText) {
          return (
            <Text
              {...passProps}
              style={style}
              onPress={(evt) => {
                passProps.onLinkPress ? passProps.onLinkPress(evt, htmlAttribs.href) : undefined
              }}>
              {children}
            </Text>
          )
        } else {
          return (
            <TouchableOpacity onPress={(evt) => {
              passProps.onLinkPress ? passProps.onLinkPress(evt, htmlAttribs.href) : undefined
            }}>
              <Text {...passProps} style={style}>
                {children}
              </Text>
            </TouchableOpacity>
          )
        }

      }
    }

    return (
      <HTML html={htmlContent} tagsStyles={htmlStyle}/>
    )
  }
}
