import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({
  container:{
    flexDirection: 'row',
    alignItems:'center',
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  icon:{
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginRight: 10,
    tintColor: 'black'
  },
  text:{
    fontSize: 14,
    color:'black',
    flex:1
  }
})
