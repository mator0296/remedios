/**
 * Automatically generated file. DO NOT MODIFY
 */
package remedios.naturales.newtea;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "remedios.naturales.newtea";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 24;
  public static final String VERSION_NAME = "2.9.0";
}
